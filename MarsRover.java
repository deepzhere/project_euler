import java.lang.*;
class MarsRover{
	int xcord,ycord;
	char dir;
	MarsRover(int x,int y,char d)
	{
	 xcord=x;ycord=y;dir=d;
	}
	public void getPosition()
	{
	 System.out.println("Current Direction "+xcord+" "+ ycord+" "+dir);
	}
	public void instructions(String input)
	{
	 for(int i=0;i<input.length();i++)
		{
		  if(input.charAt(i)=='L'){rotateLeft();}
		  else if (input.charAt(i)=='R'){rotateRight();}
		  else if (input.charAt(i)=='M'){move();}
		  else {System.out.println("Error in input");}	
		}
	} 
	private void rotateLeft()
	{
	  if(this.dir=='N'){ this.dir='W';}
	  else if(this.dir=='S'){ this.dir='E';}
	  else if(this.dir=='W'){ this.dir='S';}
	  else if(this.dir=='E'){this.dir='N';}
	}
	private void rotateRight()
	{
	  if(this.dir=='N'){ this.dir='E';}
	  else if(this.dir=='S'){ this.dir='W';}
	  else if(this.dir=='W'){ this.dir='N';}
	  else if(this.dir=='E'){ this.dir='S';}
	}
	private void move()
	{
	  if(this.dir=='N'){ this.ycord+=1;}
	  else if(this.dir=='S'){ this.ycord-=1;}
	  else if(this.dir=='W'){ this.xcord-=1;}
	  else if(this.dir=='E'){ this.xcord+=1;}
	 
	}
	  
}
